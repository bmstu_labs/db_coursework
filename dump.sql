--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2
-- Dumped by pg_dump version 15.2

-- Started on 2024-04-22 00:33:01

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: pg_database_owner
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO pg_database_owner;

--
-- TOC entry 3476 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: pg_database_owner
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 907 (class 1247 OID 80511)
-- Name: d_inn; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.d_inn AS character varying(12) NOT NULL
	CONSTRAINT d_inn_check CHECK (((VALUE)::text ~ '^[0-9]{10,12}$'::text));


ALTER DOMAIN public.d_inn OWNER TO postgres;

--
-- TOC entry 911 (class 1247 OID 80528)
-- Name: d_person_name; Type: DOMAIN; Schema: public; Owner: postgres
--

CREATE DOMAIN public.d_person_name AS character varying(100)
	CONSTRAINT d_person_name_check CHECK (((VALUE)::text ~ '^[A-Za-zА-Яа-я-]+'::text));


ALTER DOMAIN public.d_person_name OWNER TO postgres;

--
-- TOC entry 237 (class 1255 OID 80502)
-- Name: fn_check_invoice_store_good(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fn_check_invoice_store_good() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                  declare
                    rec record;
                  begin
                    FOR rec IN (SELECT es.id ent_s
								FROM
                                  invoice_goods ig
                                  join invoices i on i.id = ig.invoice_id
								  left join enterprises es on es.id = i.supplier_id
								WHERE ig.id = new.id)
                    LOOP
					  if (rec.ent_s is not null and new.store_id is null) then
						  RAISE EXCEPTION 'Не указан склад списания';
                      end if;
                    END LOOP;


                    if (new.store_id is not null) then
                      FOR rec IN(SELECT
                                    1
                                  FROM
                                    store s
                                    join invoice_goods ig on ig.id = s.invoice_goods_id
                                  WHERE
                                    s.id = new.store_id
                                    and ig.good_id != new.good_id)
                      LOOP
                        RAISE EXCEPTION 'Товар в списании не соответствует товару в приходе';
                      END LOOP;
                    end if;
                    return new;
                  end;
                $$;


ALTER FUNCTION public.fn_check_invoice_store_good() OWNER TO postgres;

--
-- TOC entry 238 (class 1255 OID 80506)
-- Name: fn_lock_change_processed_invoice(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fn_lock_change_processed_invoice() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                declare   
                    rec record;
                begin
                    FOR rec IN (SELECT 1 FROM documents d WHERE id = old.id and date_process is not null)
                    LOOP
                      RAISE EXCEPTION 'нельзя менять проведенный документ';
                    END LOOP;
                    return new;
                end;
                $$;


ALTER FUNCTION public.fn_lock_change_processed_invoice() OWNER TO postgres;

--
-- TOC entry 251 (class 1255 OID 80504)
-- Name: fn_process_document(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.fn_process_document() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
                declare   
                    rec record;
                begin
                    if (TG_OP = 'INSERT' and new.date_process is not null) then
                      RAISE EXCEPTION 'нельзя добавлять проведенный документ';
                    end if;
                    if (new.date_process is not null and old.date_process is null) then
	                  call p_store_invoice_internal(new.id);
                    elsif (new.date_process is null and old.date_process is not null) then
                      call p_unstore_invoice_internal(new.id);
                    end if;
                    return new;
                end;
                $$;


ALTER FUNCTION public.fn_process_document() OWNER TO postgres;

--
-- TOC entry 252 (class 1255 OID 80500)
-- Name: p_store_invoice_internal(integer); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.p_store_invoice_internal(IN p_invoice_id integer)
    LANGUAGE plpgsql
    AS $$
                  declare   
                    rec record;
                  begin
                    FOR rec IN (SELECT
								  ig.id, ig.qnt, ig.store_id,
								  i.supplier_id, i.recipient_id,
								  es.id ent_s, er.id ent_r,
                                  s.qnt qnt_store
								FROM
								  invoices i
								  left join invoice_goods ig on ig.invoice_id = i.id
                                  left join store s on s.id = ig.store_id
								  left join enterprises es on es.id = i.supplier_id
								  left join enterprises er on er.id = i.recipient_id
								WHERE invoice_id = p_invoice_id)
                    loop
					  if (rec.ent_s is not null) then
					    if (rec.store_id is null) then
						  RAISE EXCEPTION 'Не указан склад списания';
						else
                          if (rec.qnt_store < rec.qnt) then
                            RAISE EXCEPTION 'Недостаточное количество товара на складе';
                          end if;
	                      update store set qnt = qnt - rec.qnt where id = rec.store_id;
						end if;
				      end if;
					  if (rec.ent_r is not null) then
					    insert into store(invoice_goods_id, qnt) values(rec.id, rec.qnt);
				      end if;
                    END LOOP;
                  end;
                
                  $$;


ALTER PROCEDURE public.p_store_invoice_internal(IN p_invoice_id integer) OWNER TO postgres;

--
-- TOC entry 250 (class 1255 OID 80501)
-- Name: p_unstore_invoice_internal(integer); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.p_unstore_invoice_internal(IN p_invoice_id integer)
    LANGUAGE plpgsql
    AS $$
                  declare   
                    rec record;
                  begin
                    FOR rec IN (SELECT
								  ig.id, ig.qnt, ig.store_id,
								  i.supplier_id, i.recipient_id,
								  es.id ent_s, er.id ent_r
								FROM
								  invoices i
								  join invoice_goods ig on ig.invoice_id = i.id
								  left join enterprises es on es.id = i.supplier_id
								  left join enterprises er on er.id = i.recipient_id
								WHERE invoice_id = p_invoice_id)
                    loop
					  update store set qnt = qnt + rec.qnt where id = rec.store_id;
					  delete from store where invoice_goods_id = rec.id;
					end loop;
				  end;
                $$;


ALTER PROCEDURE public.p_unstore_invoice_internal(IN p_invoice_id integer) OWNER TO postgres;

--
-- TOC entry 253 (class 1255 OID 80559)
-- Name: test_invoices(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.test_invoices() RETURNS boolean
    LANGUAGE plpgsql
    AS $$ 
DECLARE 
	parts_id INT;
	service_id INT;
	center_id INT;
    inv_parts_service_id INT;
    inv_service_center_id INT;
    g1_id INT;
    g2_id INT;
    g3_id INT;
   
    g1_store_id INT;
    g2_store_id INT;
   
    store_rec_qnt INT;
begin
	insert into goods(id, marking, "name") values(1, 'test1', 'тестовый товар 1') RETURNING id into g1_id;
    insert into goods(id, marking, "name") values(2, 'test2', 'тестовый товар 2') RETURNING id into g2_id;
    insert into goods(id, marking, "name") values(3, 'test3', 'тестовый товар 3') RETURNING id into g3_id;

	INSERT INTO public.contragents(id) VALUES (default) RETURNING id into center_id;
    INSERT INTO public.contragents(id) VALUES (default) RETURNING id into service_id;
    INSERT INTO public.contragents(id) VALUES (default) RETURNING id into parts_id;
	
   	INSERT INTO public.companies(id, inn, legal_form_id, full_name, short_name)
	values
	    (center_id, 7719891655, 1, 'Общество с ограниченной "Обухов Автоцентр"', 'ООО "Обухов Автоцентр"'),
	    (service_id, 5003064474, 1, 'Общество с ограниченной ответственностью "Обухов Автосервис"', 'ООО "Обухов Автосервис"'),
	    (parts_id, 5032164233, 1, 'Акционерное общество "Автопартс"', 'АО "Автопартс"');
	   
	INSERT INTO public.enterprises (id)
	values (center_id), (service_id);

	INSERT INTO public.documents(id, id_doc_types, num, date, date_create)
	VALUES (default, 1, '1/2024', now(), now()) RETURNING id into inv_parts_service_id;

	INSERT INTO public.invoices(id, supplier_id, recipient_id)
	values(inv_parts_service_id, parts_id, service_id);
	
	INSERT INTO public.invoice_goods(invoice_id, good_id, qnt, store_id)
	values
		(inv_parts_service_id, g1_id, 10, null),
		(inv_parts_service_id, g2_id, 20, null),
		(inv_parts_service_id, g3_id, 5, null);

    INSERT INTO public.documents(id, id_doc_types, num, date, date_create)
	VALUES (default, 1, '1/2024', now(), now()) RETURNING id into inv_service_center_id;

	INSERT INTO public.invoices(id, supplier_id, recipient_id)
	values(inv_service_center_id, service_id, center_id);

    select count(s.id) into store_rec_qnt
    from store s join invoice_goods ig on ig.id = s.invoice_goods_id
    where ig.good_id in (g1_id, g2_id, g3_id);
	
	ASSERT store_rec_qnt = 0, 'На складе появились лишние товары';

	update documents set date_process = now() where id = inv_parts_service_id;

    select count(s.id) into store_rec_qnt
    from store s join invoice_goods ig on ig.id = s.invoice_goods_id
    where ig.good_id in (g1_id, g2_id, g3_id);
   
    ASSERT store_rec_qnt = 3, 'На складе не появились нужные товары';
   
    select s.id into g1_store_id
    from public.store s join public.invoice_goods ig on ig.id = s.invoice_goods_id 
    where ig.good_id = g1_id; 
   
    select s.id into g2_store_id
    from public.store s join public.invoice_goods ig on ig.id = s.invoice_goods_id 
    where ig.good_id = g2_id;
	
	INSERT INTO public.invoice_goods(invoice_id, good_id, qnt, store_id)
	values
		(inv_service_center_id, g1_id, 3, g1_store_id),
		(inv_service_center_id, g2_id, 4, g2_store_id);
   
    select sum(s.qnt) into store_rec_qnt
    from store s join invoice_goods ig on ig.id = s.invoice_goods_id
    where ig.good_id in (g1_id, g2_id, g3_id);
   
    ASSERT store_rec_qnt = 35, 'Неверное количество товаров на складе';
   
    update documents set date_process = now() where id = inv_service_center_id;
   
    select s.qnt into store_rec_qnt
    from store s join invoice_goods ig on ig.id = s.invoice_goods_id
    where ig.good_id = g1_id;
   
    ASSERT store_rec_qnt = 7, 'Неверное количество товаров на складе';
   
    select s.qnt into store_rec_qnt
    from store s join invoice_goods ig on ig.id = s.invoice_goods_id
    where ig.good_id = g2_id;
   
    ASSERT store_rec_qnt = 16, 'Неверное количество товаров на складе';
   
    select s.qnt into store_rec_qnt
    from store s join invoice_goods ig on ig.id = s.invoice_goods_id
    where ig.good_id = g3_id;
   
    ASSERT store_rec_qnt = 5, 'Неверное количество товаров на складе';

	return true;
end$$;


ALTER FUNCTION public.test_invoices() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 229 (class 1259 OID 80397)
-- Name: companies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.companies (
    id integer NOT NULL,
    legal_form_id integer NOT NULL,
    full_name character varying(255) NOT NULL,
    short_name character varying(255),
    inn public.d_inn NOT NULL
);


ALTER TABLE public.companies OWNER TO postgres;

--
-- TOC entry 3477 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN companies.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.companies.id IS 'Уникальный идентификатор компании';


--
-- TOC entry 3478 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN companies.legal_form_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.companies.legal_form_id IS 'Идентификатор организационно-правовой формы';


--
-- TOC entry 3479 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN companies.full_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.companies.full_name IS 'Полное наименование';


--
-- TOC entry 3480 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN companies.short_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.companies.short_name IS 'Краткое наименование';


--
-- TOC entry 3481 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN companies.inn; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.companies.inn IS 'ИНН';


--
-- TOC entry 215 (class 1259 OID 80322)
-- Name: contragents; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contragents (
    id integer NOT NULL
);


ALTER TABLE public.contragents OWNER TO postgres;

--
-- TOC entry 3483 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN contragents.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.contragents.id IS 'Уникальный идентификатор контрагента';


--
-- TOC entry 214 (class 1259 OID 80321)
-- Name: contragents_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.contragents ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.contragents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 217 (class 1259 OID 80328)
-- Name: doc_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.doc_types (
    id integer NOT NULL,
    parent_id integer,
    name character varying(100) NOT NULL
);


ALTER TABLE public.doc_types OWNER TO postgres;

--
-- TOC entry 3485 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN doc_types.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.doc_types.id IS 'Уникальный идентификатор  типа документа';


--
-- TOC entry 3486 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN doc_types.parent_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.doc_types.parent_id IS 'Идентификатор родительского типа товара';


--
-- TOC entry 3487 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN doc_types.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.doc_types.name IS 'Наименование';


--
-- TOC entry 216 (class 1259 OID 80327)
-- Name: doc_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.doc_types ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.doc_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 224 (class 1259 OID 80363)
-- Name: documents; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.documents (
    id integer NOT NULL,
    id_doc_types integer NOT NULL,
    num character varying(50) NOT NULL,
    date date NOT NULL,
    date_create timestamp without time zone NOT NULL,
    date_process timestamp without time zone
);


ALTER TABLE public.documents OWNER TO postgres;

--
-- TOC entry 3489 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN documents.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.documents.id IS 'Уникальный идентификатор  документа';


--
-- TOC entry 3490 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN documents.id_doc_types; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.documents.id_doc_types IS 'Идентификатор типа документа';


--
-- TOC entry 3491 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN documents.num; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.documents.num IS 'Номер';


--
-- TOC entry 3492 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN documents.date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.documents.date IS 'Дата документа';


--
-- TOC entry 3493 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN documents.date_create; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.documents.date_create IS 'Дата создания';


--
-- TOC entry 3494 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN documents.date_process; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.documents.date_process IS 'Дата проведения';


--
-- TOC entry 223 (class 1259 OID 80362)
-- Name: documents_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.documents ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 232 (class 1259 OID 80442)
-- Name: enterprises; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.enterprises (
    id integer NOT NULL
);


ALTER TABLE public.enterprises OWNER TO postgres;

--
-- TOC entry 3496 (class 0 OID 0)
-- Dependencies: 232
-- Name: COLUMN enterprises.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.enterprises.id IS 'Уникальный идентификатор  предприятия';


--
-- TOC entry 226 (class 1259 OID 80374)
-- Name: good_images; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.good_images (
    id integer NOT NULL,
    good_id integer NOT NULL,
    name text
);


ALTER TABLE public.good_images OWNER TO postgres;

--
-- TOC entry 3498 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN good_images.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.good_images.id IS 'Уникальный идентификатор  изображения товара';


--
-- TOC entry 3499 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN good_images.good_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.good_images.good_id IS 'Идентификатор товара';


--
-- TOC entry 3500 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN good_images.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.good_images.name IS 'Название (описание)';


--
-- TOC entry 225 (class 1259 OID 80373)
-- Name: good_images_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.good_images ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.good_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 228 (class 1259 OID 80387)
-- Name: good_rates; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.good_rates (
    id integer NOT NULL,
    good_id integer NOT NULL,
    user_id integer NOT NULL,
    rating integer NOT NULL
);


ALTER TABLE public.good_rates OWNER TO postgres;

--
-- TOC entry 3502 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN good_rates.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.good_rates.id IS 'Уникальный идентификатор  оценки';


--
-- TOC entry 3503 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN good_rates.good_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.good_rates.good_id IS 'Идентификатор товара';


--
-- TOC entry 3504 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN good_rates.user_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.good_rates.user_id IS 'Идентификатор пользователя';


--
-- TOC entry 3505 (class 0 OID 0)
-- Dependencies: 228
-- Name: COLUMN good_rates.rating; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.good_rates.rating IS 'Оценка';


--
-- TOC entry 227 (class 1259 OID 80386)
-- Name: good_rates_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.good_rates ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.good_rates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 219 (class 1259 OID 80339)
-- Name: goods; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.goods (
    id integer NOT NULL,
    marking character varying(32) NOT NULL,
    name character varying(255),
    description text,
    date_add timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.goods OWNER TO postgres;

--
-- TOC entry 3507 (class 0 OID 0)
-- Dependencies: 219
-- Name: COLUMN goods.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.goods.id IS 'Уникальный идентификатор  товара';


--
-- TOC entry 3508 (class 0 OID 0)
-- Dependencies: 219
-- Name: COLUMN goods.marking; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.goods.marking IS 'Артикул';


--
-- TOC entry 3509 (class 0 OID 0)
-- Dependencies: 219
-- Name: COLUMN goods.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.goods.name IS 'Название товара';


--
-- TOC entry 3510 (class 0 OID 0)
-- Dependencies: 219
-- Name: COLUMN goods.description; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.goods.description IS 'Описание товара';


--
-- TOC entry 3511 (class 0 OID 0)
-- Dependencies: 219
-- Name: COLUMN goods.date_add; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.goods.date_add IS 'Дата создания';


--
-- TOC entry 218 (class 1259 OID 80338)
-- Name: goods_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.goods ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.goods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 234 (class 1259 OID 80453)
-- Name: invoice_goods; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.invoice_goods (
    id integer NOT NULL,
    invoice_id integer NOT NULL,
    good_id integer NOT NULL,
    qnt numeric NOT NULL,
    store_id integer
);


ALTER TABLE public.invoice_goods OWNER TO postgres;

--
-- TOC entry 3513 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN invoice_goods.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.invoice_goods.id IS 'Уникальный идентификатор  строки в накладной';


--
-- TOC entry 3514 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN invoice_goods.invoice_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.invoice_goods.invoice_id IS 'Идентификатор накладной';


--
-- TOC entry 3515 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN invoice_goods.good_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.invoice_goods.good_id IS 'Идентификатор товара';


--
-- TOC entry 3516 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN invoice_goods.qnt; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.invoice_goods.qnt IS 'Количество товара';


--
-- TOC entry 3517 (class 0 OID 0)
-- Dependencies: 234
-- Name: COLUMN invoice_goods.store_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.invoice_goods.store_id IS 'Идентификатор ячейки склада, из которой происходит перемещение товара';


--
-- TOC entry 233 (class 1259 OID 80452)
-- Name: invoice_goods_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.invoice_goods ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.invoice_goods_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 231 (class 1259 OID 80422)
-- Name: invoices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.invoices (
    id integer NOT NULL,
    supplier_id integer NOT NULL,
    recipient_id integer NOT NULL
);


ALTER TABLE public.invoices OWNER TO postgres;

--
-- TOC entry 3519 (class 0 OID 0)
-- Dependencies: 231
-- Name: COLUMN invoices.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.invoices.id IS 'Уникальный идентификатор  накладной';


--
-- TOC entry 3520 (class 0 OID 0)
-- Dependencies: 231
-- Name: COLUMN invoices.supplier_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.invoices.supplier_id IS 'Идентификатор поставщика товара';


--
-- TOC entry 3521 (class 0 OID 0)
-- Dependencies: 231
-- Name: COLUMN invoices.recipient_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.invoices.recipient_id IS 'Идентификатор получателя товара';


--
-- TOC entry 221 (class 1259 OID 80347)
-- Name: legal_forms; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.legal_forms (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    short_name character varying(100) NOT NULL
);


ALTER TABLE public.legal_forms OWNER TO postgres;

--
-- TOC entry 3523 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN legal_forms.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.legal_forms.id IS 'Уникальный идентификатор  организационно-правовой формы';


--
-- TOC entry 3524 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN legal_forms.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.legal_forms.name IS 'Полное наименование';


--
-- TOC entry 3525 (class 0 OID 0)
-- Dependencies: 221
-- Name: COLUMN legal_forms.short_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.legal_forms.short_name IS 'Краткое наименование';


--
-- TOC entry 220 (class 1259 OID 80346)
-- Name: legal_forms_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.legal_forms ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.legal_forms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 222 (class 1259 OID 80352)
-- Name: persons; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persons (
    id integer NOT NULL,
    first_name public.d_person_name NOT NULL,
    middle_name public.d_person_name,
    last_name public.d_person_name
);


ALTER TABLE public.persons OWNER TO postgres;

--
-- TOC entry 3527 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN persons.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.persons.id IS 'Уникальный идентификатор  физического лица (внешний ключ)';


--
-- TOC entry 3528 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN persons.first_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.persons.first_name IS 'Имя';


--
-- TOC entry 3529 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN persons.middle_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.persons.middle_name IS 'Отчество';


--
-- TOC entry 3530 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN persons.last_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.persons.last_name IS 'Фамилия';


--
-- TOC entry 236 (class 1259 OID 80471)
-- Name: store; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.store (
    id integer NOT NULL,
    invoice_goods_id integer NOT NULL,
    qnt numeric NOT NULL
);


ALTER TABLE public.store OWNER TO postgres;

--
-- TOC entry 3532 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN store.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.store.id IS 'Уникальный идентификатор  ячейки склада';


--
-- TOC entry 3533 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN store.invoice_goods_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.store.invoice_goods_id IS 'Идентификатор строки накладной, согласно которой товар оказался в данной ячейке склада (внешний ключ)';


--
-- TOC entry 3534 (class 0 OID 0)
-- Dependencies: 236
-- Name: COLUMN store.qnt; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.store.qnt IS 'Количество';


--
-- TOC entry 235 (class 1259 OID 80470)
-- Name: store_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.store ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.store_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 230 (class 1259 OID 80412)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id integer NOT NULL,
    login character varying(32) NOT NULL,
    password character varying(32) NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 3536 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN users.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.users.id IS 'Уникальный идентификатор  пользователя (внешний ключ)';


--
-- TOC entry 3537 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN users.login; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.users.login IS 'Логин';


--
-- TOC entry 3538 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN users.password; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.users.password IS 'Пароль';


--
-- TOC entry 3463 (class 0 OID 80397)
-- Dependencies: 229
-- Data for Name: companies; Type: TABLE DATA; Schema: public; Owner: postgres
--


--
-- TOC entry 3269 (class 2606 OID 80401)
-- Name: companies PK_companies; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT "PK_companies" PRIMARY KEY (id);


--
-- TOC entry 3249 (class 2606 OID 80326)
-- Name: contragents PK_contragents; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contragents
    ADD CONSTRAINT "PK_contragents" PRIMARY KEY (id);


--
-- TOC entry 3260 (class 2606 OID 80367)
-- Name: documents PK_documents; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT "PK_documents" PRIMARY KEY (id);


--
-- TOC entry 3278 (class 2606 OID 80446)
-- Name: enterprises PK_enterprises; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.enterprises
    ADD CONSTRAINT "PK_enterprises" PRIMARY KEY (id);


--
-- TOC entry 3274 (class 2606 OID 80426)
-- Name: invoices PK_invoices; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invoices
    ADD CONSTRAINT "PK_invoices" PRIMARY KEY (id);


--
-- TOC entry 3258 (class 2606 OID 80356)
-- Name: persons PK_persons; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persons
    ADD CONSTRAINT "PK_persons" PRIMARY KEY (id);


--
-- TOC entry 3272 (class 2606 OID 80416)
-- Name: users PK_users; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT "PK_users" PRIMARY KEY (id);


--
-- TOC entry 3252 (class 2606 OID 80332)
-- Name: doc_types pk_doc_types; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.doc_types
    ADD CONSTRAINT pk_doc_types PRIMARY KEY (id);


--
-- TOC entry 3264 (class 2606 OID 80380)
-- Name: good_images pk_good_images; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.good_images
    ADD CONSTRAINT pk_good_images PRIMARY KEY (id);


--
-- TOC entry 3267 (class 2606 OID 80391)
-- Name: good_rates pk_good_rates; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.good_rates
    ADD CONSTRAINT pk_good_rates PRIMARY KEY (id);


--
-- TOC entry 3254 (class 2606 OID 80345)
-- Name: goods pk_goods; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.goods
    ADD CONSTRAINT pk_goods PRIMARY KEY (id);


--
-- TOC entry 3283 (class 2606 OID 80459)
-- Name: invoice_goods pk_invoice_goods; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invoice_goods
    ADD CONSTRAINT pk_invoice_goods PRIMARY KEY (id);


--
-- TOC entry 3256 (class 2606 OID 80351)
-- Name: legal_forms pk_legal_forms; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.legal_forms
    ADD CONSTRAINT pk_legal_forms PRIMARY KEY (id);


--
-- TOC entry 3286 (class 2606 OID 80477)
-- Name: store pk_store; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.store
    ADD CONSTRAINT pk_store PRIMARY KEY (id);


--
-- TOC entry 3270 (class 1259 OID 80483)
-- Name: ix_companies_legal_form_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_companies_legal_form_id ON public.companies USING btree (legal_form_id);


--
-- TOC entry 3250 (class 1259 OID 80484)
-- Name: ix_doc_types_parent_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_doc_types_parent_id ON public.doc_types USING btree (parent_id);


--
-- TOC entry 3261 (class 1259 OID 80485)
-- Name: ix_documents_id_doc_types; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_documents_id_doc_types ON public.documents USING btree (id_doc_types);


--
-- TOC entry 3262 (class 1259 OID 80486)
-- Name: ix_good_images_good_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_good_images_good_id ON public.good_images USING btree (good_id);


--
-- TOC entry 3265 (class 1259 OID 80487)
-- Name: ix_good_rates_good_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_good_rates_good_id ON public.good_rates USING btree (good_id);


--
-- TOC entry 3279 (class 1259 OID 80488)
-- Name: ix_invoice_goods_good_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_invoice_goods_good_id ON public.invoice_goods USING btree (good_id);


--
-- TOC entry 3280 (class 1259 OID 80489)
-- Name: ix_invoice_goods_invoice_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_invoice_goods_invoice_id ON public.invoice_goods USING btree (invoice_id);


--
-- TOC entry 3281 (class 1259 OID 80490)
-- Name: ix_invoice_goods_store_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_invoice_goods_store_id ON public.invoice_goods USING btree (store_id);


--
-- TOC entry 3275 (class 1259 OID 80491)
-- Name: ix_invoices_recipient_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_invoices_recipient_id ON public.invoices USING btree (recipient_id);


--
-- TOC entry 3276 (class 1259 OID 80492)
-- Name: ix_invoices_supplier_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_invoices_supplier_id ON public.invoices USING btree (supplier_id);


--
-- TOC entry 3284 (class 1259 OID 80493)
-- Name: ix_store_invoice_goods_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ix_store_invoice_goods_id ON public.store USING btree (invoice_goods_id);


--
-- TOC entry 3305 (class 2620 OID 80503)
-- Name: invoice_goods tr_check_invoice_store_good; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_check_invoice_store_good AFTER INSERT OR UPDATE OF store_id, good_id ON public.invoice_goods FOR EACH ROW EXECUTE FUNCTION public.fn_check_invoice_store_good();


--
-- TOC entry 3304 (class 2620 OID 80507)
-- Name: invoices tr_lock_change_processed_invoice; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_lock_change_processed_invoice AFTER DELETE OR UPDATE ON public.invoices FOR EACH ROW EXECUTE FUNCTION public.fn_lock_change_processed_invoice();


--
-- TOC entry 3303 (class 2620 OID 80505)
-- Name: documents tr_process_document; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_process_document AFTER INSERT OR UPDATE OF date_process ON public.documents FOR EACH ROW EXECUTE FUNCTION public.fn_process_document();


--
-- TOC entry 3292 (class 2606 OID 80554)
-- Name: companies fk_companies_contragents_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT fk_companies_contragents_id FOREIGN KEY (id) REFERENCES public.contragents(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3293 (class 2606 OID 80407)
-- Name: companies fk_companies_legal_forms_legal_form_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT fk_companies_legal_forms_legal_form_id FOREIGN KEY (legal_form_id) REFERENCES public.legal_forms(id) ON DELETE CASCADE;


--
-- TOC entry 3287 (class 2606 OID 80333)
-- Name: doc_types fk_doc_types_doc_types_parent_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.doc_types
    ADD CONSTRAINT fk_doc_types_doc_types_parent_id FOREIGN KEY (parent_id) REFERENCES public.doc_types(id);


--
-- TOC entry 3289 (class 2606 OID 80368)
-- Name: documents fk_documents_doc_types_id_doc_types; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT fk_documents_doc_types_id_doc_types FOREIGN KEY (id_doc_types) REFERENCES public.doc_types(id) ON DELETE CASCADE;


--
-- TOC entry 3298 (class 2606 OID 80447)
-- Name: enterprises fk_enterprises_companies_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.enterprises
    ADD CONSTRAINT fk_enterprises_companies_id FOREIGN KEY (id) REFERENCES public.companies(id) ON DELETE CASCADE;


--
-- TOC entry 3290 (class 2606 OID 80381)
-- Name: good_images fk_good_images_goods_good_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.good_images
    ADD CONSTRAINT fk_good_images_goods_good_id FOREIGN KEY (good_id) REFERENCES public.goods(id) ON DELETE CASCADE;


--
-- TOC entry 3291 (class 2606 OID 80392)
-- Name: good_rates fk_good_rates_goods_good_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.good_rates
    ADD CONSTRAINT fk_good_rates_goods_good_id FOREIGN KEY (good_id) REFERENCES public.goods(id) ON DELETE CASCADE;


--
-- TOC entry 3299 (class 2606 OID 80460)
-- Name: invoice_goods fk_invoice_goods_goods_good_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invoice_goods
    ADD CONSTRAINT fk_invoice_goods_goods_good_id FOREIGN KEY (good_id) REFERENCES public.goods(id) ON DELETE CASCADE;


--
-- TOC entry 3300 (class 2606 OID 80465)
-- Name: invoice_goods fk_invoice_goods_invoices_invoice_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invoice_goods
    ADD CONSTRAINT fk_invoice_goods_invoices_invoice_id FOREIGN KEY (invoice_id) REFERENCES public.invoices(id) ON DELETE CASCADE;


--
-- TOC entry 3301 (class 2606 OID 80494)
-- Name: invoice_goods fk_invoice_goods_store_store_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invoice_goods
    ADD CONSTRAINT fk_invoice_goods_store_store_id FOREIGN KEY (store_id) REFERENCES public.store(id);


--
-- TOC entry 3295 (class 2606 OID 80427)
-- Name: invoices fk_invoices_contragents_recipient_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invoices
    ADD CONSTRAINT fk_invoices_contragents_recipient_id FOREIGN KEY (recipient_id) REFERENCES public.contragents(id) ON DELETE CASCADE;


--
-- TOC entry 3296 (class 2606 OID 80432)
-- Name: invoices fk_invoices_contragents_supplier_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invoices
    ADD CONSTRAINT fk_invoices_contragents_supplier_id FOREIGN KEY (supplier_id) REFERENCES public.contragents(id) ON DELETE CASCADE;


--
-- TOC entry 3297 (class 2606 OID 80437)
-- Name: invoices fk_invoices_documents_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.invoices
    ADD CONSTRAINT fk_invoices_documents_id FOREIGN KEY (id) REFERENCES public.documents(id) ON DELETE CASCADE;


--
-- TOC entry 3288 (class 2606 OID 80357)
-- Name: persons fk_persons_contragents_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.persons
    ADD CONSTRAINT fk_persons_contragents_id FOREIGN KEY (id) REFERENCES public.contragents(id) ON DELETE CASCADE;


--
-- TOC entry 3302 (class 2606 OID 80478)
-- Name: store fk_store_invoice_goods_invoice_goods_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.store
    ADD CONSTRAINT fk_store_invoice_goods_invoice_goods_id FOREIGN KEY (invoice_goods_id) REFERENCES public.invoice_goods(id) ON DELETE CASCADE;


--
-- TOC entry 3294 (class 2606 OID 80417)
-- Name: users fk_users_persons_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_users_persons_id FOREIGN KEY (id) REFERENCES public.persons(id) ON DELETE CASCADE;


--
-- TOC entry 3482 (class 0 OID 0)
-- Dependencies: 229
-- Name: TABLE companies; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.companies TO store;
GRANT ALL ON TABLE public.companies TO admin;


--
-- TOC entry 3484 (class 0 OID 0)
-- Dependencies: 215
-- Name: TABLE contragents; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.contragents TO store;
GRANT ALL ON TABLE public.contragents TO admin;


--
-- TOC entry 3488 (class 0 OID 0)
-- Dependencies: 217
-- Name: TABLE doc_types; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.doc_types TO store;
GRANT ALL ON TABLE public.doc_types TO admin;


--
-- TOC entry 3495 (class 0 OID 0)
-- Dependencies: 224
-- Name: TABLE documents; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,TRIGGER,UPDATE ON TABLE public.documents TO store;
GRANT ALL ON TABLE public.documents TO admin;


--
-- TOC entry 3497 (class 0 OID 0)
-- Dependencies: 232
-- Name: TABLE enterprises; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.enterprises TO store;
GRANT ALL ON TABLE public.enterprises TO admin;


--
-- TOC entry 3501 (class 0 OID 0)
-- Dependencies: 226
-- Name: TABLE good_images; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.good_images TO client;
GRANT SELECT ON TABLE public.good_images TO store;
GRANT ALL ON TABLE public.good_images TO admin;


--
-- TOC entry 3506 (class 0 OID 0)
-- Dependencies: 228
-- Name: TABLE good_rates; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.good_rates TO client;
GRANT SELECT ON TABLE public.good_rates TO store;
GRANT ALL ON TABLE public.good_rates TO admin;


--
-- TOC entry 3512 (class 0 OID 0)
-- Dependencies: 219
-- Name: TABLE goods; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.goods TO client;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.goods TO store;
GRANT ALL ON TABLE public.goods TO admin;


--
-- TOC entry 3518 (class 0 OID 0)
-- Dependencies: 234
-- Name: TABLE invoice_goods; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,TRIGGER,UPDATE ON TABLE public.invoice_goods TO store;
GRANT ALL ON TABLE public.invoice_goods TO admin;


--
-- TOC entry 3522 (class 0 OID 0)
-- Dependencies: 231
-- Name: TABLE invoices; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,TRIGGER,UPDATE ON TABLE public.invoices TO store;
GRANT ALL ON TABLE public.invoices TO admin;


--
-- TOC entry 3526 (class 0 OID 0)
-- Dependencies: 221
-- Name: TABLE legal_forms; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.legal_forms TO store;
GRANT ALL ON TABLE public.legal_forms TO admin;


--
-- TOC entry 3531 (class 0 OID 0)
-- Dependencies: 222
-- Name: TABLE persons; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,UPDATE ON TABLE public.persons TO store;
GRANT ALL ON TABLE public.persons TO admin;


--
-- TOC entry 3535 (class 0 OID 0)
-- Dependencies: 236
-- Name: TABLE store; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.store TO store;
GRANT ALL ON TABLE public.store TO admin;


--
-- TOC entry 3539 (class 0 OID 0)
-- Dependencies: 230
-- Name: TABLE users; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT ON TABLE public.users TO store;
GRANT ALL ON TABLE public.users TO admin;


-- Completed on 2024-04-22 00:33:01

--
-- PostgreSQL database dump complete
--

